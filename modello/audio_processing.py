import os
import pathlib
import random
import re
import librosa
import numpy as np
import soundfile as sf
import librosa.display
import noisereduce as nr
import matplotlib.pyplot as plt

random.seed(123)

def delete_cough_from_sound(path_to_sound):
    if path_to_sound == "":
        raise Exception("No file specified in delete_cough_from_sound")

    path_to_sound = pathlib.Path(path_to_sound)

    for a in (path_to_sound.glob('*-24.wav')):
            os.remove(str(a))


def augment(f, addNoise=True, addRoll=True, timeStretch=True, nSteps=-5, already_processed=False):
    if f == "":
        raise Exception("No file specified in augment")

    out_path = f.split('.wav')[0] + '_augmented.wav'

    if already_processed:
        return out_path

    x, sr = librosa.load(f)

    # add noise
    if addNoise:
        x = x + 0.009 * np.random.normal(0, 1, len(x))

    # add roll
    if addRoll:
        x = np.roll(x, int(sr))

    # time stretch
    if timeStretch:
        factor = 0.4 + np.random.uniform(0, 0.2)
        x = librosa.effects.time_stretch(x, rate=factor)

    # pitch shifting of wav
    x = librosa.effects.pitch_shift(x, sr=sr, n_steps=nSteps)
    sf.write(out_path, x, sr)
    return out_path


def split_silence(f, min_silence_len=0.5, silence_thresh=40):
    if f == "":
        raise Exception("No file specified in split_silence")

    x, sr = librosa.load(f, mono=True)

    x = librosa.effects.trim(x, top_db=silence_thresh, ref=np.max)
    x_split = librosa.effects.split(x[0], frame_length=int(sr * min_silence_len), top_db=silence_thresh)

    out_paths = []
    for i, j in x_split:
        out_path = f.split('.wav')[0] + '_split_' + str(i) + '.wav'
        clip = x[0][i:j]
        clip = librosa.effects.trim(clip, top_db=silence_thresh)[0]
        if len(clip) > 0.5 * sr:
            if len(clip) < 1 * sr:
                clip = librosa.util.fix_length(clip, size=1*sr)

            sf.write(out_path, clip, sr)
            out_paths.append(out_path)

    return out_paths


def get_ambient_sound(ambient_path):
    return ambient_path + "/" + random.choice(os.listdir(ambient_path))

def get_cough_sound(path):
    while 1:
        f = random.choice(os.listdir(path))
        if not "split" in str(f):
            audio, sr = librosa.load(f)
            audio_split = librosa.effects.split(audio, frame_length=int(sr * 0.3), top_db=48)
            print(len(audio_split))
            if len(audio_split) > 6:
                return path + "/" + f


def audio_mixing(path1, path2="", ambient_path="", top_db=30, overwrite=True):
    if path1 == "":
        raise Exception("No file specified in audio_mixing")
    
    if ambient_path == "":
        raise Exception("No ambient path specified in audio_mixing")

    x1, sr1 = librosa.load(path1)

    if path2 == "":
        ambient = get_ambient_sound(ambient_path)
    else:
        ambient = path2

    x2, sr2 = librosa.load(ambient, sr=sr1)

    x1 = librosa.effects.trim(x1, top_db=top_db)[0]
    x2 = librosa.effects.trim(x2, top_db=top_db)[0]
    
    while (len(x2)<len(x1)):
        x2 = np.append(x2, x2)

    x2 = librosa.util.fix_length(x2, size=len(x1))/2.5

    x = (x1+x2)

    if not overwrite: 
        path1 = path1.split('.wav')[0] + '_mix.wav' 

    sf.write(path1, x, sr1)

    return path1


