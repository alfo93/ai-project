import sys, os
import pathlib
import sklearn
import warnings
import itertools
import numpy as np
from sklearn.utils import shuffle
from imblearn.over_sampling import SMOTE
import tensorflow as tf
import keras_tuner as kt
import matplotlib.pyplot as plt
import tensorflow_datasets as tfds
from keras.applications.mobilenet_v2 import preprocess_input
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
warnings.filterwarnings("ignore")
tf.random.set_seed(42)


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
warnings.filterwarnings("ignore")

# Path setting
mel_path = 'mel_img/'
scg_path = 'scg_img_1/'
data_dir = pathlib.Path(mel_path)

# Model Settings
img_height = 224
img_width = 224

SEED = 1
TEST_SPLIT = 0.25
VALIDATION_SPLIT = 0.20
MAX_EPOCHS = 30
EARLY_STOPPING_PATIENCE = 10

# Printing settings
print("Image height:", img_height)
print("Image width:", img_width)
print("Dataset split: ", TEST_SPLIT)
print("Seed: ", SEED)
print("Max epochs: ", MAX_EPOCHS)
print("Early stopping patience: ", EARLY_STOPPING_PATIENCE)
print("")


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()


# LOADING DATA
sys.stdout = open(os.devnull, 'w') 

train_ds = tf.keras.utils.image_dataset_from_directory(
    data_dir,
    validation_split=TEST_SPLIT,
    subset="training",
    shuffle=True,
    seed=SEED,
    image_size=(img_height, img_width),
    batch_size=1000000,
    label_mode='binary'
)

test_ds = tf.keras.utils.image_dataset_from_directory(
    data_dir,
    validation_split=TEST_SPLIT,
    subset="validation",
    seed=SEED,
    shuffle=True,
    image_size=(img_height, img_width),
    batch_size=1000000,
    label_mode='binary'
)

sys.stdout = sys.__stdout__

# Splitting data from labels

# Training data
x_train = None
y_train = None

for image, label in tfds.as_numpy(train_ds):
    x_train = image
    y_train = label

# Test data
x_test = None
y_test = None

for image, label in tfds.as_numpy(test_ds):
    x_test = image
    y_test = label

n_samples = len(y_train)
n_classes = 2

# Count each class frequency
class_count = [0] * n_classes

for y in y_train:
    if y == 0:
        class_count[0] += 1
    else:
        class_count[1] += 1

weights = sklearn.utils.class_weight.compute_class_weight('balanced', classes=np.unique(y_train), y=y_train[:, 0])
class_weight = {0: weights[0], 1: weights[1]}


# Model builder for Keras Tuner
class ModelBuilder(kt.HyperModel):
    def build(self, hp):
        learning_rate = hp.Choice("learning_rate", [0.001,0.0001,0.00001])
        dropout_value = hp.Choice("dropout_value", [0.3,0.4,0.5])

        img_shape = (img_width, img_height) + (3,)
        base_model = tf.keras.applications.MobileNetV2(input_shape=img_shape,
                                                    include_top=False,
                                                    weights='imagenet')

        image_batch, label_batch = next(iter(train_ds))
        feature_batch = base_model(image_batch)

        base_model.trainable = True
        global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
        feature_batch_average = global_average_layer(feature_batch)
        prediction_layer = tf.keras.layers.Dense(1, )
        prediction_layer(feature_batch_average)

        inputs = tf.keras.Input(shape=(img_width, img_height, 3))
        x = preprocess_input(inputs)
        x = base_model(x, training=False)
        x = global_average_layer(x)
        x = tf.keras.layers.Dropout(dropout_value)(x)
        outputs = prediction_layer(x)
        model = tf.keras.Model(inputs, outputs)

        model.compile(optimizer=tf.keras.optimizers.RMSprop(learning_rate=learning_rate),
                    loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                    metrics=["accuracy"])

        return model

    def fit(self, hp, model, *args, **kwargs):
        return model.fit(
            *args,
            use_multiprocessing=True,
            class_weight=class_weight,
            workers=os.cpu_count()-2,
            batch_size=hp.Choice("batch_size", [8,16]),
            **kwargs,
        )


tuner = kt.Hyperband(ModelBuilder(),
                     objective='val_accuracy',
                     max_epochs=MAX_EPOCHS,
                     factor=3,
                     directory='model_tuning',
                     project_name='optimizer')

stop_early = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', patience=EARLY_STOPPING_PATIENCE)

# Searching for best model
tuner.search(x_train, y_train, epochs=MAX_EPOCHS, validation_split=VALIDATION_SPLIT, callbacks=[stop_early])

# Tuning the best model
best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]

# Hyperparameter tuning results
print("The hyperparameter search is complete")
print(best_hps["dropout_value"])

model = tuner.hypermodel.build(best_hps)
history = model.fit(x_train, y_train, epochs=MAX_EPOCHS, validation_split=VALIDATION_SPLIT)

acc_per_epoch = history.history['accuracy']
best_epoch = acc_per_epoch.index(max(acc_per_epoch)) + 1
print('\nBest epoch: %d \n' % (best_epoch,))

hypermodel = tuner.hypermodel.build(best_hps)

# Retrain the model
hist = hypermodel.fit(x_train, y_train, epochs=30, validation_split=VALIDATION_SPLIT)

eval_result = hypermodel.evaluate(x_test, y_test)
print("\n[test loss, test accuracy, mse]:", eval_result)

y_pred = model.predict(x_test)
y_pred = [1 if v >= 0 else 0 for v in y_pred]
y_pred = np.round(y_pred)

print("\n Training Accuracy and Validation Accuracy")
plt.plot(hist.history['accuracy'], label='acc')
plt.plot(hist.history['val_accuracy'], label='val acc')
plt.title("Acc vs Val_acc")
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.legend()
plt.show()

print("\n  - Confusion matrix:")
print(confusion_matrix(y_test, y_pred))

print("\n  - Classification report:")
print(classification_report(y_true=y_test, y_pred=y_pred))

print("\n Validation Loss and Training Loss")
plt.style.use('ggplot')
plt.plot(hist.history['loss'], label='loss')
plt.plot(hist.history['val_loss'], label='val loss')
plt.title("Loss vs Val_Loss")
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend()
plt.show()
